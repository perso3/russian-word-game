import re


def open_dico(file_name):
    file = open(file_name, encoding='utf-8', mode='r')
    text = file.read()

    new_dico = text.split("\n")
    new_dico.remove("")
    return new_dico


def contains(letters, words):
    letters_dico = {}

    for letter in letters:
        if letter in letters_dico.keys():
            letters_dico[letter] += 1
        else:
            letters_dico[letter] = 1

    result = []
    for word in words:
        temp_word = word

        next_word = False
        for letter in word:
            if letter not in letters:
                next_word = True

        if not next_word:
            pick = True
            for letter in letters_dico:
                # print(word, letter, temp_word.count(letter), letters_dico[letter])
                if temp_word.count(letter) > letters_dico[letter]:
                    # print("refuse")
                    pick = False

            if pick:
                # print(word)
                result.append(word)

    return result


def match(pattern, words):
    result = []
    for word in words:
        if re.fullmatch(pattern, word) is not None:
            result.append(word)
    return result


if __name__ == '__main__':
    dico = open_dico("russian.txt")
    dico = contains(["а", "б", "л", "о", "ч"], dico)
    dico = match("^а.+", dico)
    print(dico)
